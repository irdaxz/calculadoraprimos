package net.iescierva.dam17_03.calculadoraprimos;

import java.util.ArrayList;

public class Calculadora {
    private  ArrayList<Integer> primos = new ArrayList<>();
    public Calculadora(){

    }


    public  int calcular(int numero){
        //Si el arrayList está vacío
        if(primos.size()<2){
            primos.add(2);
            primos.add(3);
            primos.add(5);
        }

        int inicial = primos.get(primos.size()-1);
        while (numero>primos.size()){
            int i = 2;
            inicial++;
            while (inicial%i != 0)
                i++;

            if(inicial == i){
                primos.add(i);
            }
        }
        return primos.get(numero-1);
    }
}
