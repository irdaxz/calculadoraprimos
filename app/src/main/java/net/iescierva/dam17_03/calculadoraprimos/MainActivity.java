package net.iescierva.dam17_03.calculadoraprimos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements  View.OnClickListener{

    EditText numero;
    Button btnPrimo;
    TextView resultado;
    Calculadora calculadora = new Calculadora();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        numero = findViewById(R.id.etPosicion);
        btnPrimo = findViewById(R.id.btnPrimo);
        resultado = findViewById(R.id.tvResultado);
        btnPrimo.setOnClickListener(this);
    }

    public void onClick (View v){
        try {
            int intNumero = Integer.parseInt(numero.getText().toString());
            int resultadoPrimo = calculadora.calcular(intNumero);
            String resultadoPrimoString = Integer.toString(resultadoPrimo);
            resultado.setText(resultadoPrimoString);

        } catch (Exception e){
            Toast.makeText(getApplicationContext(), "¡Debes escribir solo números!", Toast.LENGTH_SHORT).show();
        }
    }
}
